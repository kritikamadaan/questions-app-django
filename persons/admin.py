from django.contrib import admin
from .models import Person
from .models import Group
from .models import Membership
from .models import Runner,Fruit,Musician


# Register your models here.


admin.site.register(Person)
admin.site.register(Group)
admin.site.register(Membership)
admin.site.register(Runner)
admin.site.register(Fruit)
admin.site.register(Musician)


